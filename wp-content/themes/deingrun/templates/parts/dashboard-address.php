<?php $current_user = wp_get_current_user();

$birth_day_arr = array();
$full_name_arr = array();

$first_name = get_user_meta($current_user->ID,'first_name',true);
$last_name = get_user_meta($current_user->ID,'last_name',true);

$birth_day = get_user_meta($current_user->ID,'dg_birth_day',true);
$birth_month = get_user_meta($current_user->ID,'dg_birth_month',true);
$birth_year = get_user_meta($current_user->ID,'dg_birth_year',true);

$birth_day_arr[] = $birth_day;
$birth_day_arr[] = $birth_month;
$birth_day_arr[] = $birth_year;

if(!empty($first_name)) {
    $full_name_arr[] = $first_name;
}

if(!empty($last_name)) {
    $full_name_arr[] = $last_name;
}

$billing_address = wc_get_account_formatted_address();
$shipping_address = wc_get_account_formatted_address('shipping'); ?>

<div class="row">
    <div class="col-sm-4">
        <div class="dg-dashboard-column">
            <h4>Persönliche Daten</h4>
            <div class="dg-dashboard-column-inner">
                <p><?php echo join(' ',$full_name_arr); ?><br>
                <?php echo join('.',$birth_day_arr); ?><br>
                <?php echo $current_user->data->user_email; ?></p>
            </div>
            <a href="<?php echo wc_customer_edit_account_url(); ?>" class="btn">DATEN ÄNDERN</a>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="dg-dashboard-column">
            <h4>Rechnungsadresse</h4>
            <div class="dg-dashboard-column-inner">
                <p><?php echo !empty($billing_address)?$billing_address:esc_html_e( 'You have not set up this type of address yet.', 'woocommerce' ); ?></p>
            </div>
            <a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-address', 'billing' ) ); ?>" class="btn">DATEN ÄNDERN</a>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="dg-dashboard-column">
            <h4>Lieferaddresse</h4>
            <div class="dg-dashboard-column-inner">
                <p><?php echo $shipping_address; ?></p>
            </div>
            <a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-address', 'shipping' ) ); ?>" class="btn">DATEN ÄNDERN</a>
        </div>
    </div>
</div>