<?php

$date_birth_data = dg_get_date_birth_data();

?>

<div style="clear: both;"></div>
<br>
<h3>Geburtsdatum</h3>
<div class="row">
    <div class="col-sm-4">
        <?php woocommerce_form_field('dg_birth_day',array(
            'type' => 'select',
            'options' => $date_birth_data['days']
        )); ?>
    </div>
    <div class="col-sm-4">
        <?php woocommerce_form_field('dg_birth_month',array(
            'type' => 'select',
            'options' => $date_birth_data['month']
        )); ?>
    </div>
    <div class="col-sm-4">
        <?php woocommerce_form_field('dg_birth_year',array(
            'type' => 'select',
            'options' => $date_birth_data['years']
        )); ?>
    </div>
</div>
<br>
<h3>Ihre Adresse</h3>
<div class="row">
    <div class="col-sm-6">

        <?php woocommerce_form_field('shipping_address_1',array(
            'label' => 'Straße und Nummer',
            'required' => true
        )); ?>

    </div>
    <div class="col-sm-6">

        <?php woocommerce_form_field('shipping_postcode',array(
            'label' => __('Postcode','woocommerce'),
            'required' => true
        )); ?>

    </div>
</div>
<div class="row">
    <div class="col-sm-6">

        <?php woocommerce_form_field('shipping_city',array(
            'label' => __('City','deingrun'),
            'required' => true
        )); ?>

    </div>
    <div class="col-sm-6">

        <?php woocommerce_form_field('shipping_country',array(
            'type' => 'country',
            'label' => __('Country','woocommerce'),
            'required' => true
        )); ?>

    </div>
</div>

<?php woocommerce_form_field('agreement_different_address',array(
    'type' => 'checkbox',
    'label' => 'Die Lieferadresse weicht von der Rechnungsadresse ab.',
    'required' => false,
    'class' => array('form-row-margin-bottom0')
)); ?>

<p><span class="text-required">*</span> hierbei handelt es sich um ein Pflichtfeld</p>