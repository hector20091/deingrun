<div class="row">
    <div class="col-sm-6">

        <?php woocommerce_form_field('dg_anrede',array(
            'type' => 'select',
            'label' => 'Andere',
            'required' => true,
            'options' => array(
                ''   => 'Andere',
                'mr' => 'Herr',
                'ms' => 'Frau'
            )
        )) ?>

    </div>
</div>
<div class="row">
    <div class="col-sm-6">

        <?php woocommerce_form_field('shipping_first_name',array(
            'label' => __('First name','woocommerce'),
            'required' => true
        )); ?>

    </div>
    <div class="col-sm-6">

        <?php woocommerce_form_field('shipping_last_name',array(
            'label' => __('Last name','woocommerce'),
            'required' => true
        )); ?>

    </div>
</div>
