<?php

function planner_post_type_init() {
    $args = array(
        'labels' => array(
            'name' => __( 'Planner' ),
            'singular_name' => __( 'Planner' )
        ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'planners' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'thumbnail' )
    );

    register_post_type( 'dg_planner', $args );

    register_taxonomy(
        'dg_planner_categories',
        'dg_planner',
        array(
            'label' => __( 'Categories' ),
            'rewrite' => array( 'slug' => 'planner-cat' ),
            'hierarchical' => true,
        )
    );
}

add_action('init','planner_post_type_init');