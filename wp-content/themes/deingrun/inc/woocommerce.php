<?php
add_theme_support( 'woocommerce' );


remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
add_filter('woocommerce_breadcrumb_defaults', 'my_parameter_breadcrumbs');
function my_parameter_breadcrumbs() {
	return array(
	  'delimiter'   => false,
	  'wrap_before' => '<ul class="breadcrumb">',
	  'wrap_after'  => '</ul>',
	  'before'      => '<li>',
	  'after'       => '</li>',
	  'home'        => false,
  );
}

remove_action('woocommerce_after_shop_loop_item','woocommerce_template_loop_product_link_close', 5);
add_action('woocommerce_after_shop_loop_item_title','woocommerce_template_loop_product_link_close', 5);
function woocommerce_template_loop_product_link_close() {
	echo '<div class="product-footer">';
}
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating',5);
add_action('woocommerce_after_shop_loop_item_title', 'product_weight', 6);
function product_weight() {
	global $product;
	$weight = get_post_meta( get_the_ID(), '_weight', true);
	if ( ! empty( $weight ) ) {
		$desc = '<div class="product-description" data-weight="'.$weight.'">';
		$desc .= '<b>Inhalt:</b> '.$weight.' Gramm ';
		$desc .= '(<span class="price-kg"></span> € * /<br> 1000 Gramm)</div>';
		echo $desc;
	}
}

remove_action('woocommerce_before_shop_loop_item_title','woocommerce_template_loop_product_thumbnail', 10);
remove_action('woocommerce_shop_loop_item_title','woocommerce_template_loop_product_title', 10);
function woocommerce_template_loop_product_link_open() {
	global $product;
	$link = apply_filters( 'woocommerce_loop_product_link', get_the_permalink(), $product );
	echo '<a href="' . esc_url( $link ) . '" class="woocommerce-LoopProduct-link woocommerce-loop-product__link" style="background-image:url('.wp_get_attachment_image_url($product->image_id, 'medium').')"></a>';
	echo '<h2><a href="'.esc_url( $link ).'">' . get_the_title() . '</a></h2>';
}

if ( ! function_exists( 'cart_link' ) ) {
 	function cart_link() { 
 		echo	'<a href="'. wc_get_cart_url() .'" class="bask cart_totals btn"><i></i><span class="price">'. WC()->cart->cart_contents_total.' &euro;</span></a>';
 	}
}

/*single product*/
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
remove_action( 'woocommerce_before_single_product', 'wc_print_notices', 10 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 15 );
add_action( 'woocommerce_single_product_summary', 'product_weight', 20 );
add_action( 'woocommerce_single_product_summary', function(){
	echo '<div class="terms">inkl. MwSt. zzgl. Versandkosten</div>';
}, 25 );
add_action( 'woocommerce_single_product_summary', 'product_weight', 20 );

function woo_change_account_order() {
    $myorder = array(
// для изменения очередности - меняйте местами
        'edit-account'       => __( 'Persönliche Daten', 'woocommerce' ),
        'edit-address'       => __( 'Adresse', 'woocommerce' ),
        'payment-methods'    => __( 'Zahlungsarten', 'woocommerce' ),
        'orders'             => __( 'Bestellungen', 'woocommerce' ),
        //'mein-beet'    			 => __( 'Mein Beet', 'woocommerce' ),
    );
    return $myorder;
}
//add_filter ( 'woocommerce_account_menu_items', 'woo_change_account_order' );

//add_action( 'woocommerce_edit_account_form', 'deingrun_print_user_frontend_fields', 10 );

add_action('woocommerce_register_form','dengrun_register_fields_after',10);
function dengrun_register_fields_after() {
    theme_get_template('templates/parts/register-fields-after');
}

add_action('woocommerce_register_form_start','dengrun_register_fields_before', 10);
function dengrun_register_fields_before() {
    theme_get_template('templates/parts/register-fields-before');
}

function dg_get_account_fields() {
    return array(
        'dg_anrede' => array(
            'type'  => 'select',
            'label' => __('Salutation', 'deingrun'),
        ),
        'shipping_first_name'         => array(
            'type'  => 'text',
            'label' => __('First name', 'woocommerce'),
        ),
        'shipping_last_name'          => array(
            'type'  => 'text',
            'label' => __('Last name', 'woocommerce'),
        ),
        'dg_birth_day'                => array(
            'type'  => 'text',
            'label' => __('Birth Day', 'deingrun'),
        ),
        'dg_birth_month'              => array(
            'type'  => 'text',
            'label' => __('Birth Month', 'deingrun'),
        ),
        'dg_birth_year'               => array(
            'type'  => 'text',
            'label' => __('Birth Year', 'deingrun'),
        ),
        'shipping_address_1'          => array(
            'type'  => 'text',
            'label' => __('Address', 'deingrun'),
        ),
        'shipping_postcode'           => array(
            'type'  => 'text',
            'label' => __('Postcode', 'woocommerce'),
        ),
        'shipping_city'               => array(
            'type'  => 'text',
            'label' => __('City', 'deingrun'),
        ),
        'shipping_country'            => array(
            'type'  => 'text',
            'label' => __('Country', 'woocommerce'),
        ),
        'agreement_different_address' => array(
            'type'  => 'checkbox',
            'label' => __('Agreement', 'deingrun'),
        )
    );
}

function deingrun_validate_user_frontend_fields( $errors ) {
    $fields = dg_get_account_fields();

    foreach ( $fields as $key => $field_args ) {
        /*if ( empty( $field_args['required'] ) ) {
            continue;
        }*/

        if($key == 'agreement_different_address') {
            continue;
        }

        if ( empty( $_POST[ $key ] ) ) {
            $message = sprintf( __( '%s is a required field.', 'deingrun' ), '<strong>' . $field_args['label'] . '</strong>' );
            $errors->add( $key, $message );
        }
    }

    return $errors;
}

add_filter( 'woocommerce_registration_errors', 'deingrun_validate_user_frontend_fields', 10 );
//add_filter( 'woocommerce_save_account_details_errors', 'deingrun_validate_user_frontend_fields', 10 );

function iconic_save_account_fields( $customer_id ) {
    $save_fields = array();
    $fields = dg_get_account_fields();

    foreach ( $fields as $key => $field_args ) {
        $value    = isset( $_POST[ $key ] ) ? sanitize_text_field($_POST[ $key ]) : '';

        if(!empty($value)) {
            if ($key == 'shipping_first_name') {
                $save_fields['first_name'] = $value;
            }

            if ($key == 'shipping_last_name') {
                $save_fields['last_name'] = $value;
            }

            $save_fields[$key] = $value;
        }
    }

    if(isset($_POST['account_first_name'])) {
        $save_fields['first_name'] = sanitize_text_field($_POST['account_first_name']);
    }

    if(isset($_POST['account_last_name'])) {
        $save_fields['last_name'] = sanitize_text_field($_POST['account_last_name']);
    }


    if(sizeof($save_fields) > 0) {
        foreach ($save_fields as $meta_key => $meta_value) {
            update_user_meta( $customer_id, $meta_key, $meta_value );
        }
    }
}

add_action( 'woocommerce_created_customer', 'iconic_save_account_fields' ); // register/checkout
//add_action( 'personal_options_update', 'iconic_save_account_fields' ); // edit own account admin
//add_action( 'edit_user_profile_update', 'iconic_save_account_fields' ); // edit other account admin
add_action( 'woocommerce_save_account_details', 'iconic_save_account_fields' ); // edit WC account

function deingrun_dashboard_address() {
    theme_get_template('templates/parts/dashboard-address');
}
add_action('woocommerce_account_dashboard','deingrun_dashboard_address',10);

function deingrun_account_menu_items($items) {

    $edit_account = $items['edit-account'];
    $edit_address = $items['edit-address'];

    unset($items['edit-account']);
    //unset($items['edit-address']);

    $items = wcs_array_insert_after( 'dashboard', $items, 'edit-account', $edit_account );
    $items = wcs_array_insert_after( 'edit-account', $items, 'edit-address', $edit_address );

    return $items;
}
add_filter('woocommerce_account_menu_items','deingrun_account_menu_items',10,1);

function dg_get_date_birth_data() {
    $return = array();

    $days = array(''=>'Tag');
    $month = array(''=>'Monat');
    $years = array(''=>'Jahr');
    $years_range = array_combine(range(date("Y"), 1910), range(date("Y"), 1910));

    foreach (range(1, 31) as $item) {
        $days[$item] = $item;
    }

    foreach (range(1, 12) as $item) {
        $month[$item] = $item;
    }

    foreach ($years_range as $item) {
        $years[$item] = $item;
    }

    $return['days'] = $days;
    $return['month'] = $month;
    $return['years'] = $years;

    return $return;
}

add_filter('woocommerce_save_account_details_required_fields', 'dg_save_account_details_required_fields' );
function wc_save_account_details_required_fields( $required_fields ){
    unset( $required_fields['account_display_name'] );
    return $required_fields;
}

add_filter( 'woocommerce_locate_template', 'dg_multistep_locate_template', 20, 3 );
function dg_multistep_locate_template( $template, $template_name, $template_path ){
    if( 'checkout/form-checkout.php' != $template_name )
        return $template;
    $template = TEMPLATEPATH . '/templates/multistep/form-checkout.php';
    return $template;
}

function wpse_131562_redirect() {
    if (
        ! is_user_logged_in()
        //&& (is_cart() || is_checkout())
        && is_checkout()
    ) {
        // feel free to customize the following line to suit your needs
        wp_redirect(get_permalink( get_option('woocommerce_myaccount_page_id') ));
        exit;
    }
}
add_action('template_redirect', 'wpse_131562_redirect');



function deingrun_registration_redirect($redirect) {
    //echo '<pre>';print_r(WC()->cart);echo '</pre>';
    //echo '<pre>';print_r($redirect);echo '</pre>';
    if ( ! WC()->cart->is_empty() ) {
        $redirect = wc_get_checkout_url();
    }

    //echo '<pre>';print_r($redirect);echo '</pre>';die;

    return $redirect;
}
add_action('woocommerce_registration_redirect', 'deingrun_registration_redirect', 10);