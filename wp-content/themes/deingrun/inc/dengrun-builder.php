<?php

include 'aq_resizer.php';
include 'post-types.php';

function dg_session() {
    if(!session_id()) {
        session_start();
    }
}
add_action('init', 'dg_session', 1);

function get_one_space_number() {
    return 0.2;
}

function get_subscription_product_id() {
    return 104;
}

function grid_items() {
    return 33;
}

function search_revisions($dataArray, $search_value, $key_to_search, $other_matching_value = null, $other_matching_key = null) {
    // This function will search the revisions for a certain value
    // related to the associative key you are looking for.

    if(empty($dataArray)) return array();

    $keys = array();
    foreach ($dataArray as $key => $cur_value) {
        if ($cur_value[$key_to_search] == $search_value) {
            if (isset($other_matching_key) && isset($other_matching_value)) {
                if ($cur_value[$other_matching_key] == $other_matching_value) {
                    $keys[] = $key;
                }
            } else {
                // I must keep in mind that some searches may have multiple
                // matches and others would not, so leave it open with no continues.
                $keys[] = $key;
            }
        }
    }
    return $keys;
}

function theme_get_template( $template = '', $args = array(), $echo = true ) {
    ob_start();
    include TEMPLATEPATH . '/' . $template . '.php';
    $content = ob_get_contents();
    ob_end_clean();

    if($echo) {
        echo $content;
    } else {
        return $content;
    }
}

function add_usr_custom_session($product_name, $values, $cart_item_key ) {
    global $post;

    $grid_products = $_SESSION['planner'];
    $grid_product_html_list = '';

    if($values['product_id'] == get_subscription_product_id()) {

        $grid_product_html_list .= '<div class="cart-sub-products-wrapper">';

        foreach ($grid_products as $id => $grid_product) {
            $post = get_post($id);
            $featured_image_url = get_the_post_thumbnail_url($post);
            $featured_image_url_cropped = aq_resize($featured_image_url,200,200,true,true,true);
            ob_start(); ?>

            <div class="sub-product-item">
                <div class="sub-product-thumbnail">
                    <img src="<?php echo $featured_image_url_cropped; ?>">
                </div>
                <div class="sub-product-description">
                    <h5><?php echo get_the_title($post); ?></h5>
                    <?php echo apply_filters('the_content',$post->post_content); ?>
                    <div class="sub-product-cart-info">
                        <p><?php _e('Space','deingrun'); ?>: <?php echo $grid_product['space']; ?> m&sup2;</p>
                        <p><?php _e('Count','deingrun'); ?>: <?php echo $grid_product['count']; ?></p>
                    </div>
                </div>
            </div>

            <?php $grid_product_html_list .= ob_get_contents();
            ob_end_clean();
            wp_reset_postdata();
        }

        $grid_product_html_list .= '</div>';

        $product_name = $product_name . "<br />" . $grid_product_html_list;
    }

    return $product_name;

}
add_filter('woocommerce_cart_item_name','add_usr_custom_session',1,3);

function update_custom_price( $cart_object ) {
    //echo '<pre>';print_r($cart_object);echo '</pre>';die;
    $subscription_product_id = get_subscription_product_id();
    foreach ( $cart_object->cart_contents as $cart_item_key => $value ) {
        // Version 2.x
        //$value['data']->price = $value['_custom_options']['custom_price'];
        // Version 3.x / 4.x
        if($value['product_id'] == $subscription_product_id) {
            $product = wc_get_product($subscription_product_id);
            $product_price = $product->get_price();
            $planner_products = $_SESSION['planner'];

            if($planner_products > 0) {
                $space = 0;
                foreach ($planner_products as $planner_product) {
                    $space = $space + ($planner_product['space'] * $planner_product['count']);

                }

                $value['data']->set_price($product_price*$space);
            }
        }

    }
}
add_action( 'woocommerce_before_calculate_totals', 'update_custom_price', 1, 1 );

add_action('wp_ajax_planner_add_to_cart', 'planner_add_to_cart_callback');
add_action('wp_ajax_nopriv_planner_add_to_cart', 'planner_add_to_cart_callback');
function planner_add_to_cart_callback() {
    $result = array();

    if(sizeof($_SESSION['planner']) > 0) {

        $subscription_product_id = get_subscription_product_id();

        $allow_to_add = true;

        // allow only one subscription product
        foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
            if ($cart_item['product_id'] == get_subscription_product_id()) {
                $allow_to_add = false;
                break;
            }
        }

        if ($allow_to_add) {
            WC()->cart->add_to_cart($subscription_product_id);
        }

        $result['status'] = true;
        $result['redirect'] = wc_get_cart_url();
    } else {
        $result['status'] = false;
        $result['message'] = 'No any zum beet products. Please add them.';
    }

    wp_send_json($result);
}

add_action('wp_ajax_planner_add_to_session', 'planner_add_to_session_callback');
add_action('wp_ajax_nopriv_planner_add_to_session', 'planner_add_to_session_callback');
function planner_add_to_session_callback() {
    $data = $_POST['data'];

    $_SESSION['planner'][$data['id']] = $data;

    //echo '<pre>';print_r($_SESSION);echo '</pre>';

    die;
}

add_action('wp_ajax_planner_update_item_quantity', 'planner_update_item_quantity_callback');
add_action('wp_ajax_nopriv_planner_update_item_quantity', 'planner_update_item_quantity_callback');
function planner_update_item_quantity_callback() {
    $config = $_POST['config'];
    $id = $config['id'];

    $_SESSION['planner'][$id] = $config;
    die;
}

add_action('wp_ajax_planner_remove_item', 'planner_remove_item_callback');
add_action('wp_ajax_nopriv_planner_remove_item', 'planner_remove_item_callback');
function planner_remove_item_callback() {
    $result = array(
        'refresh' => false
    );

    $config = $_POST['config'];
    $id = $config['id'];

    unset($_SESSION['planner'][$id]);

    if(sizeof($_SESSION['planner']) == 0) {
        foreach( WC()->cart->cart_contents as $prod_in_cart ) {
            if($prod_in_cart['product_id'] == get_subscription_product_id()) {
                WC()->cart->remove_cart_item($prod_in_cart['key']);
            }
        }
        $result['refresh'] = true;
    }

    wp_send_json($result);
}

add_action('wp_ajax_planner_check_exist', 'planner_check_exist_callback');
add_action('wp_ajax_nopriv_planner_check_exist', 'planner_check_exist_callback');
function planner_check_exist_callback() {
    $result = array();

    $planner_items = $_SESSION['planner'];
    $ids = array_keys($planner_items);

    if(!in_array($_POST['id'],$ids)) {
        $result['status'] = true;
    } else {
        $result['status'] = false;
    }

    wp_send_json($result);
}

function dg_cart_item_quantity($product_quantity, $cart_item_key, $cart_item) {
    if($cart_item['product_id'] == get_subscription_product_id()) {
        $product_quantity = 1;
    }

    return $product_quantity;
}
add_filter('woocommerce_cart_item_quantity','dg_cart_item_quantity',10,3);

function dg_pre_get_posts_query( $q ) {
    $exclude = array();
    $exclude[] = get_subscription_product_id();

    $q->set( 'post__not_in', $exclude );
}
add_action( 'woocommerce_product_query', 'dg_pre_get_posts_query' );

function dg_add_planner_items_to_order_item_meta($item_id, $values) {
    $planner_items = $_SESSION['planner'];

    wc_add_order_item_meta($item_id,'planner_items',$planner_items);
    add_post_meta($item_id,'planner_items',$planner_items);
}
//add_action('woocommerce_add_order_item_meta','dg_add_planner_items_to_order_item_meta',1,2);

function tshirt_order_meta_handler( $item, $cart_item_key, $values, $order ) {
    $planner_items = $_SESSION['planner'];

    wc_add_order_item_meta($order->id,'planner_items',$planner_items);
}
add_action('woocommerce_checkout_create_order_line_item','tshirt_order_meta_handler',20,4);

function dg_clear_planner_session() {
    unset($_SESSION['planner']);
}
add_action( 'woocommerce_cart_emptied', 'dg_clear_planner_session', 10);

function dg_cart_after_remove_product($cart_item_key, $cart) {
    $product_id = $cart->removed_cart_contents[ $cart_item_key ]['product_id'];

    if($product_id == get_subscription_product_id()) {
        unset($_SESSION['planner']);
    }
}
add_action( 'woocommerce_cart_item_removed', 'dg_cart_after_remove_product', 10, 2 );

function dg_add_my_account_order_actions( $actions, $order ) {
    $show_button = false;

    foreach ($order->get_items() as $item) {
        $item_data = $item->get_data();

        if($item_data['product_id'] == get_subscription_product_id()) {
            $show_button = true;
            break;
        }
    }

    if($show_button) {

        $actions['name'] = array(
            'url'  => get_permalink( wc_get_page_id( 'myaccount' ) ) . 'view-order-grid/' . $order->id,
            'name' => __('View Grid Products','deingrun'),
        );

    }

    return $actions;
}
//add_filter( 'woocommerce_my_account_my_orders_actions', 'dg_add_my_account_order_actions', 10, 2 );

function dg_body_class($classes) {
    global $post;

    if($post->post_name == 'view-order-grid') {
        $classes[] = 'woocommerce-account woocommerce-page';
    }

    //unset($_SESSION['planner']);

    return $classes;
}
add_filter('body_class','dg_body_class');

/*function clear_zum_beet_data() {
    unset($_SESSION['planner']);
}
add_action('woocommerce_cleanup_sessions','clear_zum_beet_data',10);

add_filter('wc_session_expiring', 'filter_ExtendSessionExpiring', 20);

add_filter('wc_session_expiration', 'filter_ExtendSessionExpired', 20);

function filter_ExtendSessionExpiring($seconds)
{
    return 1 * 60;
}

function filter_ExtendSessionExpired($seconds)
{
    return 1 * 60;
}*/