<?php $categories = get_terms('dg_planner_categories',array(
    'hide_empty' => true
));

if(sizeof($categories) > 0) { ?>

    <div class="col-lg-auto">
        <a href="#" class="toggler"><i class="fas fa-bars"></i> Category</a>
        <div class="filters column">
            <ul class="categories list-unstyled">

                <?php foreach ($categories as $cat) { ?>

                    <li class="cat-item"><a href="<?php echo get_term_link($cat); ?>"><?php echo $cat->name; ?></a></li>

                <?php } ?>

            </ul>
        </div>
    </div>

<?php } ?>