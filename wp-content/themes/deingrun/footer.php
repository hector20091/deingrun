<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package deingrun
 */

?>

	<div class="page-footer">
		<footer class="footer">
			<div class="container">
				<div class="row">
					<div class="col-xl-3 col-lg-2"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo"><img src="<?= get_template_directory_uri() ?>/img/logo.png" alt="<?php bloginfo( 'name' ); ?>" class="img-responsive"></a></div>
					<div class="col-lg-5 col-xl-4 col-md-6">
						<h3 class="h3">Navigation</h3>
						<div class="nav-menu">
							<?php
								wp_nav_menu( array(
									'theme_location' => 'menu-2',
									'menu_id'        => 'footer-menu',
								) );
							?>
						</div>
					</div>
					<div class="col-xl-5 col-lg-5 col-md-6">
						<h3 class="h3">Zahlungsarten</h3>
						<ul class="list-inline payment">
							<li><img src="<?= get_template_directory_uri() ?>/img/maestro.png" alt=""></li>
							<li><img src="<?= get_template_directory_uri() ?>/img/paypal.png" alt=""></li>
							<li><img src="<?= get_template_directory_uri() ?>/img/visa.png" alt=""></li>
							<li><img src="<?= get_template_directory_uri() ?>/img/mastercard.png" alt=""></li>
						</ul>
						<p>* Alle Preise inkl. gesetzl. Mehrwertsteuer zzgl. Versandkosten</p>
					</div>
				</div>
				<div class="text-center copyright">© Copyright 2018 DEINGRÜN.DE</div>
			</div>
		</footer>
	</div>
</div>
<div id="tooltip"></div>
<?php wp_footer(); ?>

</body>
</html>
