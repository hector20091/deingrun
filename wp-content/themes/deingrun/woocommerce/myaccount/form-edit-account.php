<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_edit_account_form' );

$date_birth_data = dg_get_date_birth_data();

$current_user_id = get_current_user_id();

$anrede = get_user_meta($current_user_id,'dg_anrede',true);

$birth_day = get_user_meta($current_user_id,'dg_birth_day',true);
$birth_month = get_user_meta($current_user_id,'dg_birth_month',true);
$birth_year = get_user_meta($current_user_id,'dg_birth_year',true);

?>

<h3>Persönliche Daten</h3>
<p>Ihre persönlichen daten ändern:</p>

<form class="woocommerce-EditAccountForm edit-account" action="" method="post">

	<?php do_action( 'woocommerce_edit_account_form_start' ); ?>

    <div class="row">
        <div class="col-sm-4">

            <?php woocommerce_form_field('dg_anrede',array(
                'type' => 'select',
                'label' => __('Salutation', 'deingrun'),
                'required' => true,
                'options' => array(
                    ''   => __('Salutation', 'deingrun'),
                    'mr' => 'Herr',
                    'ms' => 'Frau'
                )
            ), $anrede); ?>

        </div>
        <div class="col-sm-4">
            <p class="woocommerce-form-row form-row">
                <label for="account_first_name"><?php esc_html_e( 'First name', 'woocommerce' ); ?> <span class="required">*</span></label>
                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name" value="<?php echo esc_attr( $user->first_name ); ?>" />
            </p>
        </div>
        <div class="col-sm-4">
            <p class="woocommerce-form-row form-row">
                <label for="account_last_name"><?php esc_html_e( 'Last name', 'woocommerce' ); ?> <span class="required">*</span></label>
                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name" id="account_last_name" value="<?php echo esc_attr( $user->last_name ); ?>" />
            </p>
        </div>
    </div>

    <!--<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
        <label for="account_display_name"><?php /*esc_html_e( 'Display name', 'deingrun' ); */?>&nbsp;<span class="required">*</span></label>
        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_display_name" id="account_display_name" value="<?php /*echo esc_attr( $user->display_name ); */?>" /> <span><em><?php /*esc_html_e( 'This will be how your name will be displayed in the account section and in reviews', 'deingrun' ); */?></em></span>
    </p>-->

    <div class="row">
        <div class="col-sm-8">
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <label for="account_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>
                <input type="email" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" value="<?php echo esc_attr( $user->user_email ); ?>" />
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8">
            <label>Geburtsdatum</label>
            <div class="row">
                <div class="col-sm-4">
                    <?php woocommerce_form_field('dg_birth_day',array(
                        'type' => 'select',
                        'options' => $date_birth_data['days']
                    ), $birth_day); ?>
                </div>
                <div class="col-sm-4">
                    <?php woocommerce_form_field('dg_birth_month',array(
                        'type' => 'select',
                        'options' => $date_birth_data['month']
                    ), $birth_month); ?>
                </div>
                <div class="col-sm-4">
                    <?php woocommerce_form_field('dg_birth_year',array(
                        'type' => 'select',
                        'options' => $date_birth_data['years']
                    ), $birth_year); ?>
                </div>
            </div>
        </div>
    </div>

    <h4><?php esc_html_e( 'Passwort', 'woocommerce' ); ?></h4>

    <div class="row">
        <div class="col-sm-4">
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <label for="password_current"><?php esc_html_e( 'Ihr aktuelles Passwort', 'woocommerce' ); ?></label>
                <input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_current" id="password_current" />
                <span><em><?php _e('Leave blank to leave unchanged','deingrun'); ?></em></span>
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <label for="password_1"><?php esc_html_e( 'New password', 'woocommerce' ); ?></label>
                <input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_1" id="password_1" />
                <span><em><?php _e('Leave blank to leave unchanged','deingrun'); ?></em></span>
            </p>
        </div>
        <div class="col-sm-4">
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <label for="password_2"><?php esc_html_e( 'Confirm new password', 'woocommerce' ); ?></label>
                <input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_2" id="password_2" />
            </p>
        </div>
    </div>

    <p><span class="text-required">*</span> hierbei handelt es sich um ein Pflichtfeld</p>

	<div class="clear"></div>

	<?php do_action( 'woocommerce_edit_account_form' ); ?>

	<p>
		<?php wp_nonce_field( 'save_account_details' ); ?>
		<button type="submit" class="woocommerce-Button btn" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"><?php esc_html_e( 'Save changes', 'woocommerce' ); ?></button>
		<input type="hidden" name="action" value="save_account_details" />
	</p>

	<?php do_action( 'woocommerce_edit_account_form_end' ); ?>
</form>

<?php do_action( 'woocommerce_after_edit_account_form' ); ?>
