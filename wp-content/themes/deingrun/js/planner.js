var Planner = function () {
    var totalSpace = 0;
    var filledSpaceId = '#filled-space';
    var loading = false;

    return {
        init: function () {
            Planner.events();
            Planner.calcFilledSpace();
        },
        
        events: function () {
            jQuery(document)
                .on('click', '.beet-item .drag-link .item-count', function () {
                    var that = jQuery(this);
                    var $box = that.closest('.drag-link');

                    $box.addClass('product-edit-qty');

                    return false;
                })

                .on('click', '.beet-item .drag-link .set-item-count-wrapp button', function () {
                    var that = jQuery(this);
                    var $box = that.closest('.drag-link');
                    var config = $box.data('config');
                    var productCount = jQuery('input',$box).val();

                    if(!jQuery.isNumeric(productCount)) {
                        alert('Value is not valid');
                        return false;
                    }

                    if(productCount<1) {
                        alert('Value can not be less 1');
                        jQuery('input',$box).val(1);
                        return false;
                    }

                    config.count = parseInt(productCount);

                    $box.data('config',config);

                    jQuery('.item-count',$box).text(config.count);
                    $box.removeClass('product-edit-qty');

                    Planner.updateItemQty(config, Planner.calcFilledSpace);

                    return false;
                })

                .on('click', '.beet-item .item-remove', function () {
                    var that = jQuery(this);
                    var $box = that.closest('.drag-link');
                    var config = $box.data('config');

                    if(confirm(dg_config.string.planner_remove_item)) {
                        $box.remove();
                        Planner.removeItem(config, function (response) {
                            if(response.refresh) {
                                location.reload();
                            } else {
                                Planner.calcFilledSpace();
                            }
                        });
                    }

                    return false;
                })
            
                .on('click', '[data-add_to_beet]', function () {
                    var that = jQuery(this);
                    var config = that.data('config');
                    var $wrap = that.closest('.herb');
                    var $box = jQuery('.drag-link',$wrap);
                    var except = [];

                    var position = 1;

                    jQuery('.beet-wrap .beet .beet-item .drag-link').each(function (i,e) {
                        var $el = jQuery(e);
                        var config = $el.data('config');

                        except.push(config.position);
                    });

                    console.log(except);

                    config.position = Planner.generateRandomPosition(1,dg_config.planner.grid_items,except);

                    //config.position = Planner.getRandomArbitrary(1,dg_config.planner.grid_items);

                    that.data('config',config);
                    $box.attr('data-config',JSON.stringify(config));

                    console.log($box);

                    Planner.canPutItToGrid($box.data('config'), function (response) {
                        if (response.status) {
                            Planner.addToSession(that.data('config'), function () {
                                Planner.putElementToGrid($box, config);
                            });
                        } else {
                            alert(dg_config.string.planner_exist_in_grid);
                        }
                    });
                })
            ;
        },

        generateRandomPosition: function (min, max, except) {
            except.sort(function(a, b) {
                return a - b;
            });
            var random = Math.floor(Math.random() * (max - min + 1 - except.length)) + min;
            var i;
            for (i = 0; i < except.length; i++) {
                if (except[i] > random) {
                    break;
                }
                random++;
            }
            return random;
            //return Planner.getRandomArbitrary(1,dg_config.planner.grid_items);
        },

        putElementToGrid: function ($box, config) {
            //console.log($box);
            //var config = $box.data('config');

            jQuery('.beet-wrap .beet > .beet-item').eq(config.position-1).html($box.clone());
        },

        putOnGrid: function (e) {
            var $box = jQuery(e.target);
            var boxConfig = $box.data('config');
            var $beetItem = $box.closest('.beet-item');

            boxConfig.position = $beetItem.index() + 1;

            $box.data('config',boxConfig);

            Planner.addToSession($box.data('config'));
        },

        getRandomArbitrary: function (min, max) {
            return parseInt(Math.random() * (max - min) + min);
        },
        
        addToSession: function (config, callback) {
            jQuery.ajax({
                type: 'post',
                url: dg_config.ajax_url,
                data : {
                    action: 'planner_add_to_session',
                    data: config
                },
                beforeSend: Planner.loader(true),
                success: function(response) {
                    console.log(response);

                    if(typeof callback == 'function') {
                        callback(response);
                    }

                    Planner.calcFilledSpace();
                    Planner.loader(false);
                }
            });
        },
        
        calcFilledSpace: function () {
            var filledSpace = 0;

            jQuery('.beet .beet-item').each(function (i,e) {
                var that = jQuery(e);

                if(jQuery('.drag-link',that).length) {
                    var itemConfig = jQuery('.drag-link',that).data('config');
                    var itemSpace = itemConfig.space;
                    var itemCount = itemConfig.count;

                    //itemConfig.position = i+1;

                    var productSpace = parseFloat(itemSpace)*parseInt(itemCount);

                    filledSpace = filledSpace+productSpace;
                }
            });

            totalSpace = filledSpace;

            jQuery(filledSpaceId).text(parseFloat(totalSpace).toFixed(3));
        },

        canPutItToGrid: function (e, callback) {
            if(e.target == undefined) {
                id = e.id;
            } else {
                var config = jQuery(e.target).data('config');

                id = config.id;
            }

            jQuery.ajax({
                type: 'post',
                url: dg_config.ajax_url,
                data : {
                    action: 'planner_check_exist',
                    id: id
                },
                beforeSend: Planner.loader(true),
                success: function(response) {
                    callback(response);
                    Planner.loader(false);
                }
            });
        },

        _canPutItToGrid: function (event) {
            var all_ids = [];

            jQuery('.beet .beet-item').each(function (i,e) {
                var that = jQuery(e);

                if(jQuery('.drag-link',that).length) {
                    var itemConfig = jQuery('.drag-link',that).data('config');

                    all_ids.push(itemConfig.id);
                }
            });

            console.log(all_ids);
            console.log(all_ids.length);

            if(all_ids.length == 0) return true;

            if(!all_ids.includes(jQuery(event.target).data('config').id)) {
                return true;
            } else {
                return false;
            }
        },

        updateItemQty: function (config, callback) {
            jQuery.ajax({
                type: 'post',
                url: dg_config.ajax_url,
                data : {
                    action: 'planner_update_item_quantity',
                    config: config
                },
                beforeSend: Planner.loader(true),
                success: function(response) {
                    callback();
                    Planner.loader(false);
                }
            });
        },

        removeItem: function (config, callback) {
            jQuery.ajax({
                type: 'post',
                url: dg_config.ajax_url,
                data : {
                    action: 'planner_remove_item',
                    config: config
                },
                beforeSend: Planner.loader(true),
                success: function(response) {
                    callback(response);
                    Planner.loader(false);
                }
            });
        },

        addToCart: function () {
            jQuery.ajax({
                type: 'post',
                url: dg_config.ajax_url,
                data : {
                    action: 'planner_add_to_cart',
                    space: totalSpace
                },
                beforeSend: Planner.loader(true),
                success: function(response) {
                    if(response != undefined) {
                        if(response.status) {
                            location.href = response.redirect;
                        } else {
                            alert(response.message);
                        }
                        Planner.loader(false);
                    }
                }
            });
        },
        
        loader: function (status) {
            var $loader = jQuery('<div id="planner-loader"><div>' + dg_config.string.planner_processing + '</div></div>');
            var $body = jQuery('body');

            if(!jQuery('#planner-loader').length) {
                $body.append($loader);
            }

            if(status) {
                jQuery('#planner-loader').show();
            } else {
                jQuery('#planner-loader').remove();
            }

        }
    }
}();

jQuery(window).on('load', Planner.init());