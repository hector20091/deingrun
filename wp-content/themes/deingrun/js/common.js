$(function() {

	$(window).on('scroll', function() {
		var header = $('.page-header').height();
		
		if($(this).scrollTop() > header) {
			$('.page-header').addClass('sticky');
		}else{
			$('.page-header').removeClass('sticky');
		}
	});

	$('.page-banner').owlCarousel({
		items: 1,
		loop: true,
		animateOut: 'fadeOut',
		smartSpeed:450,
		autoplay: true
	});

	if ($(document).height() <= $(window).height()){
		$(".page-footer").css('position','fixed');
	}

	if($('#products').length){
		$('.product').each( function() {
			var count = $(this).find('.product-description').data('weight');
			var price = $(this).find('.amount').text();
			price = price.replace(/[^\d\,]/g, '').replace(',','.');
			var total = (price * 1000 / count).toFixed(2);
			total = total.replace('.',',')
			$(this).find('.price-kg').text(total);
		});
	}

	$('woocommerce-product-gallery__image a').magnificPopup({type:'image'});

});
