<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package deingrun
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title><?php bloginfo('name'); ?><?php wp_title('|'); ?></title>
	<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>

    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/all-ie-only.css" />
    <![endif]-->

</head>

<body <?php body_class(); ?>>

<div id="page" class="page-wrap">
	<div class="page-header">
		<header id="masthead" class="site-header">
			<div class="container">
				<div class="row justify-content-between">
					<div class="col-sm-auto">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo"><img src="<?= get_template_directory_uri() ?>/img/logo.png" alt="<?php bloginfo( 'name' ); ?>" class="img-responsive"></a>
					</div>
					<div class="col align-self-center">
						<div class="row justify-content-end">
							<div class="col-sm-auto">
								<ul class="toolbar list-inline">
									<li>
										<?php if(is_user_logged_in()) : ?>
											<!--<a href="<?php /*echo wp_logout_url(get_permalink()); */?>" class="btn login btn-gray"><i></i><span>Logout</span></a>-->
											<a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>" class="btn my-account-btn btn-gray"><i></i><span><?php _e('My Account','deingrun'); ?></span></a>
											<?php else:?>
											<a href="<?php echo wc_customer_edit_account_url() ?>" class="btn login btn-gray"><i></i><span><?php _e('Login','deingrun'); ?></span></a>
										<?php endif; ?>
									</li>
									<li><?php is_cart()? '' : cart_link() ?></li>
								</ul>
							</div>
							<div class="col-sm-auto">
								<a href="#" class="toggler"><i class="fas fa-bars"></i></a>
								<div class="nav-bar">
									<?php
										wp_nav_menu( array(
											'theme_location' => 'menu-1',
											'menu_id'        => 'primary-menu',
										) );
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
	</div>

