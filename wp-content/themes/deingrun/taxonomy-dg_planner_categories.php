<?php get_header();

$product = wc_get_product(get_subscription_product_id());
$price = $product->get_price(); ?>

    <div class="breadcrumbs-wrap">
        <div class="container">
            <ul class="breadcrumb"><li>Gemüse-Planer</li></ul>
        </div>
    </div>

    <div class="page-content">
        <div class="section">
            <div class="container">
                <p>Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure dolor in hend</p>

                <?php get_template_part('template-parts/beet-grid'); ?>

            </div>

            <div class="container">
                <div class="beet-info">
                    <div class="row justify-content-between">
                        <div class="col-sm-auto"><h6 class="cat-title">Wähle deine Pflanzen aus:</h6></div>
                        <div class="col-sm-auto"><div class="beet-count">Belegte : <span id="filled-space">0</span> m&sup2;</div></div>
                    </div>
                </div>
                <div class="row">

                    <?php get_sidebar('beet'); ?>

                    <div class="col">

                        <?php if ( have_posts() ) : ?>

                            <ul class="herbs row">

                                <?php while ( have_posts() ) : the_post(); ?>

                                    <?php get_template_part('template-parts/beet-list-item'); ?>

                                <?php endwhile; ?>

                            </ul>

                            <div class="row herbs-footer justify-content-between">
                                <div class="col-sm-9">
                                    <div class="description">1m² für einen Gesamtpreis von <?php echo wc_price( $price ); ?> monatlich</div>
                                </div>
                                <div class="col-sm-auto"><a href="javascript:;" onclick="Planner.addToCart();" class="btn">In den warenkorb</a></div>
                            </div>

                        <?php else: ?>

                            <p>Nothing Found!</p>

                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>


    </div>

<?php get_footer(); ?>
