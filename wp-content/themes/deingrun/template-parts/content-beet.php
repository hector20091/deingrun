<?php global $post;

$planner_list = get_posts(array(
    'post_type'   => 'dg_planner',
    'post_status' => 'publish',
    'numberposts' => -1
));

$product = wc_get_product(get_subscription_product_id());

?>

<div class="breadcrumbs-wrap">
	<div class="container">
		<ul class="breadcrumb"><li>Gemüse-Planer</li></ul>
	</div>
</div>
<div class="page-content">
	<div class="section">
		<div class="container">

            <?php while ( have_posts() ) : the_post();

                the_content();

            endwhile; // End of the loop. ?>

            <?php get_template_part('template-parts/beet-grid'); ?>

		</div>
		<div class="container">
			<div class="beet-info">
				<div class="row justify-content-between">
					<div class="col-sm-auto"><h6 class="cat-title">Wähle deine Pflanzen aus:</h6></div>
                    <div class="col-sm-auto"><div class="beet-count">Belegte: <span id="filled-space">0</span> m&sup2;</div></div>
				</div>
			</div>
			<!--<div class="row">-->

                <?php //get_sidebar('beet'); ?>

				<!--<div class="col">-->

                    <?php if(sizeof($planner_list) > 0) { ?>

                        <ul class="herbs row">

                            <?php foreach ($planner_list as $post) { setup_postdata($post); ?>

                                <?php get_template_part('template-parts/beet-list-item'); ?>

                            <?php } wp_reset_postdata(); ?>

                        </ul>

                        <div class="row herbs-footer justify-content-between">
                            <div class="col-sm-9">
                                <div class="description">1m² für einen Gesamtpreis von <?php echo wc_price( $product->get_price() ); ?> monatlich</div>
                            </div>
                            <div class="col-sm-auto"><a href="javascript:;" onclick="Planner.addToCart();" class="btn">In den warenkorb</a></div>
                        </div>

                    <?php } else { ?>

                        <p><?php _e('Nothing Found!','deingrun'); ?></p>

                    <?php } ?>

				<!--</div>-->
			<!--</div>-->
		</div>
	</div>
	

</div>