<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package safetysquare
 */

?>

<div class="page-banner owl-carousel">
	<?php $fields = CFS()->get(); ?>
	<?php	if( !empty($fields['slides'] ) ): $slides = $fields['slides'];?>
		<?php foreach($slides as $slide) { ?>
			<div class="item" style="background-image: url(<?=$slide['image']?>);">
				<div class="container">
					<div class="item-in row align-items-center">
						<div class="col-md-11">
							<div class="h1"><?php echo $slide['title']?></div>
							<div class="h2"><?php echo $slide['subtitle']?></div>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	<?php endif; ?>
</div>

<div class="page-content">
	<div class="entry section">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-9">
					<?php while ( have_posts() ) : the_post(); the_content();?>
				</div>
				<div class="col-lg-4 col-md-3 collage-wrap">
					<div class="collage-img"><img src="<?php the_post_thumbnail_url(); ?>" alt=""></div>
				</div>
				<?php endwhile;?>
			</div>
		</div>
	</div>

	<div class="section zum-menu">
		<div class="container">
			<div class="row">
				<?php $query = new WP_Query(array('post_type' => 'page', 'post__in' => array( 14,7 ))); ?>
				<?php while($query->have_posts()): $query->the_post(); ?>
				<div class="col-md-6">
					<a href="<?= get_the_permalink() ?>" class="item-wrap">
						<img src="<?php the_post_thumbnail_url(); ?>" alt="" class="img-responsive">
						<span class="text-wrap row">
							<span class="col align-self-center">
								<span class="h2"><?php the_title(); ?></span>
								<span class="subtitle"><?php echo CFS()->get('subtitle'); ?></span>
							</span>
						</span>
					</a>
				</div>
				<?php endwhile; wp_reset_query();?>
			</div>
		</div>
	</div>

</div>