<?php $space = CFS()->get('space');

$thumbnail = 'http://via.placeholder.com/250x250';
if(has_post_thumbnail()) {
    $thumbnail = get_the_post_thumbnail_url();
}

$data = array();

$data['id'] = get_the_ID();
$data['space'] = $space;
$data['count'] = 1;
$data['position'] = 0;

?>

<li class="col-lg-2">
    <div class="post-11 herb">
        <span href="javascript:;" data-config='<?php echo json_encode($data); ?>' class="drag-link" style="background-image: url(<?php echo $thumbnail; ?>);">
            <span class="item-count-wrapp">
                <span class="item-count"><?php echo $data['count']; ?></span>
                <span class="set-item-count-wrapp"><input type="text" value="<?php echo $data['count']; ?>" step="1" min="1"><button class="btn">OK</button></span>
            </span>
            <span class="item-remove">x</span>
        </span>
        <div class="tooltip hidden">
            <div class="title">Infos zu <?php the_title(); ?></div>

            <?php the_content(); ?>

        </div>
        <span class="btn"><?php the_title(); ?></span>

        <?php if(!empty($space)) { ?>

            <div class="herb-footer">
                <div class="herb-description"><?php echo $space; ?> m&sup2;</div>
            </div>

        <?php } ?>

        <button data-config='<?php echo json_encode($data); ?>' data-add_to_beet type="button" class="btn btn-primary btn-block">Add To Beet</button>

    </div>
</li>