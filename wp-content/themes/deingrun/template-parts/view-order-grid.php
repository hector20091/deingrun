<?php /* Template Name: Order Grid Products */
get_header();

global $wp_query, $post;

$order_id = $wp_query->query['page'];
$order = new WC_Subscription($order_id);

$order_data = $order->get_data();
$order_total = $order_data['total'];
$planner_items = wc_get_order_item_meta($order_id,'planner_items',true);

$filled_space = 0;
$total_price = 0;

if(!is_user_logged_in()) return;

if(get_current_user_id() != $order_data['customer_id']) return;

if(empty($planner_items)) return; ?>

    <div class="page-content">
        <div class="container">

            <div class="section row no-gutters justify-content-between">

                <?php do_action( 'woocommerce_account_navigation' ); ?>

                <div class="woocommerce-MyAccount-content">

                    <div class="daingrun-order-details">

                        <?php theme_get_template('template-parts/beet-grid',array(
                            'items' => $planner_items
                        )); ?>

                        <div class="beet-info">
                            <div class="row justify-content-between">
                                <div class="col-sm-auto"><h6 class="cat-title">Wähle deine Pflanzen aus:</h6></div>
                                <div class="col-sm-auto"><div class="beet-count">Belegte: <span id="filled-space"><?php echo $filled_space; ?></span> m&sup2;</div></div>
                            </div>
                        </div>

                        <table class="order-items-table">

                            <?php foreach ($planner_items as $id => $item) {
                                $post = get_post($id);
                                setup_postdata($post);

                                $item_total_space = $item['space']*$item['count'];

                                $filled_space = $filled_space + $item_total_space;

                                $art_number = get_post_meta($id,'art_number',true); ?>

                                <tr>
                                    <td class="item-thumbnail" width="120">
                                        <div class="item-thumb-box">
                                            <?php $thumbnail_url = get_the_post_thumbnail_url();
                                            $thumbnail_cropped = aq_resize($thumbnail_url,200,200,true,true,true); ?>

                                            <img src="<?php echo $thumbnail_cropped; ?>">
                                            <span class="item-count"><?php echo $item['count']; ?></span>
                                        </div>
                                    </td>
                                    <td class="item-data">
                                        <div class="item-title"><?php the_title(); ?></div>

                                        <?php if(!empty($art_number)) { ?>

                                            <p><b>Art. Nr.:</b> <?php echo $art_number; ?></p>

                                        <?php } ?>

                                        <p><b>Stück m²:</b> <?php echo $item['space']; ?> m²</p>
                                        <p><b>Gesamt m²:</b> <?php echo $item_total_space; ?> m²</p>
                                    </td>
                                    <td class="item-description">
                                        <h5>Beschreibung</h5>

                                        <?php the_content(); ?>

                                    </td>
                                </tr>

                            <?php } wp_reset_postdata(); ?>

                        </table>

                        <div class="row herbs-footer justify-content-between">
                            <div class="col-sm-9">
                                <div class="description"><?php echo $filled_space; ?> m² für einen Gesamtpreis von <?php echo wc_price($order_total); ?> monatlich</div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>
    </div>

<?php get_footer(); ?>