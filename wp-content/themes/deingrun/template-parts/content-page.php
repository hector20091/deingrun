<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package deingrun
 */

?>
<div class="container">
<?php
	the_content();
?>
</div>
	

	
