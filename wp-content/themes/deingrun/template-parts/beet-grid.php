<?php global $post;

$planner_items = $_SESSION['planner'];

$view_only = false;
if(isset($args['items'])) {
    $planner_items = $args['items'];
    $view_only = true;
} ?>

<div class="beet-wrap">
    <div class="beet">

        <?php $i = 1; while ($i <= grid_items()) { ?>

            <div class="beet-item">

                <?php

                $key = search_revisions($planner_items,$i,'position');
                if(sizeof($key) > 0) {
                    $data = $planner_items[$key[0]];
                    $item_id = $data['id'];

                    if ($item_id) {
                        $post = get_post($item_id);
                        setup_postdata($post);

                        $thumbnail = 'http://via.placeholder.com/250x250';
                        if (has_post_thumbnail()) {
                            $thumbnail = get_the_post_thumbnail_url();
                        } ?>

                        <span href="javascript:;" data-config='<?php echo json_encode($data); ?>' class="drag-link" style="background-image: url(<?php echo $thumbnail; ?>);">

                            <span class="item-count-wrapp">
                                <span class="item-count<?php if($view_only){echo '-alt';} ?>"><?php echo $data['count']; ?></span>

                                <?php if(!$view_only) { ?>

                                    <span class="set-item-count-wrapp"><input type="text" value="<?php echo $data['count']; ?>"
                                                                              step="1" min="1"><button class="btn">OK</button></span>

                                <?php } ?>

                            </span>

                            <?php if(!$view_only) { ?>

                                <span class="item-remove">x</span>

                            <?php } ?>

                        </span>

                        <?php wp_reset_postdata();

                    }
                } ?>

            </div>

        <?php $i++; } ?>

    </div>
</div>