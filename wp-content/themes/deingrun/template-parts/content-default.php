<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package safetysquare
 */

?>
<div class="breadcrumbs-wrap">
	<div class="container">
		<?php woocommerce_breadcrumb(); ?>
	</div>
</div>
<div class="page-content">
	<div class="section">
		<div class="container">
			<?php while ( have_posts() ) : the_post(); the_content();endwhile;?>
		</div>
	</div>
</div>