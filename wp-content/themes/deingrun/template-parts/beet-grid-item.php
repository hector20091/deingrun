<?php $space = CFS()->get('space');

$thumbnail = 'http://via.placeholder.com/250x250';
if(has_post_thumbnail()) {
    $thumbnail = get_the_post_thumbnail_url();
}

$data = array();

$data['id'] = get_the_ID();
$data['space'] = $space;
$data['count'] = 1;
$data['position'] = 0;

?>

<span href="javascript:;" data-config='<?php echo json_encode($data); ?>' class="drag-link" style="background-image: url(<?php echo $thumbnail; ?>);">
    <span class="item-count-wrapp">
        <span class="item-count"><?php echo $data['count']; ?></span>
        <span class="set-item-count-wrapp"><input type="number" value="<?php echo $data['count']; ?>" step="1" min="1"><button>OK</button></span>
    </span>
    <span class="item-remove">x</span>
</span>