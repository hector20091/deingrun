<?php

include 'inc/dengrun-builder.php';

/**
 * deingrun functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package deingrun
 */

if ( ! function_exists( 'deingrun_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function deingrun_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on deingrun, use a find and replace
		 * to change 'deingrun' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'deingrun', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'deingrun' ),
			'menu-2' => esc_html__( 'Footer', 'deingrun' )
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'deingrun_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'deingrun_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function deingrun_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'deingrun_content_width', 640 );
}
add_action( 'after_setup_theme', 'deingrun_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function deingrun_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'deingrun' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'deingrun' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'deingrun_widgets_init' );

function my_widget_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Category', 'deingrun' ),
		'id'            => 'category_widget',
		'description'   => esc_html__( 'Add widgets here.', 'deingrun' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h2 class="hidden">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'my_widget_init' );

/**
 * WC_Widget_Product_Categories widget override
 */
add_action( 'widgets_init', 'override_woocommerce_widgets', 15 );

function override_woocommerce_widgets() {
  if ( class_exists( 'WC_Widget_Product_Categories' ) ) {
    unregister_widget( 'WC_Widget_Product_Categories' );
     include_once( 'widgets/widget-product_categories.php' );
     register_widget( 'Custom_WC_Widget_Product_Categories' );
  }
}

/**
 * Enqueue scripts and styles.
 */
function deingrun_scripts() {
    wp_register_script('dg-planner', get_template_directory_uri() . '/js/planner.js', array('jquery'), rand(), true );
	wp_enqueue_style( 'my-style', get_template_directory_uri() . '/css/main.min.css', array(), rand() );
	wp_enqueue_style( 'deingrun-style', get_stylesheet_uri() );
	wp_enqueue_script( 'main-scripts', get_template_directory_uri() . '/js/scripts.min.js', array('jquery','dg-planner'), rand(), true );
	//wp_enqueue_script( 'deingrun-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	//wp_enqueue_script( 'deingrun-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

    wp_localize_script('dg-planner','dg_config',array(
        'ajax_url' => admin_url('admin-ajax.php'),
        'planner' => array(
            'grid_items' => grid_items()
        ),
        'string' => array(
            'planner_remove_item'   => 'Sind Sie sicher, dass Sie diesen Artikel löschen wollen?',
            'planner_processing'    => 'wird geladen...',
            'planner_exist_in_grid' => 'Bereits im Raster vorhanden'
        )
    ));
}
add_action( 'wp_enqueue_scripts', 'deingrun_scripts' );

/*** Load WooCommerce compatibility file. */ 
require get_template_directory() . '/inc/woocommerce.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Remove password strength check.
 */
function dg_remove_password_strength() {
    wp_dequeue_script( 'wc-password-strength-meter' );
}
add_action( 'wp_print_scripts', 'dg_remove_password_strength', 10 );